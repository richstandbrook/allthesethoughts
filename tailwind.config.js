module.exports = {
  theme: {
    extend: {
      colors: {
        purple: {
          100: "#bcb8c4",
          200: "#797189",
          300: "#4c4262",
          400: "#281F5B",
          500: "#20133B",
          600: "#1D1135",
          700: "#130B23",
          800: "#0E091B",
          900: "#0A0612"
        },
        red: {
          100: "#FFEDEF",
          200: "#FFD3D7",
          300: "#FFB9BF",
          400: "#FF848E",
          500: "#FF4F5E",
          600: "#E64755",
          700: "#992F38",
          800: "#73242A",
          900: "#4D181C"
        },
        blue: {
          100: "#E9F3FD",
          200: "#C7E2FB",
          300: "#A5D0F8",
          400: "#62ADF3",
          500: "#1F8AEE",
          600: "#1C7CD6",
          700: "#13538F",
          800: "#0E3E6B",
          900: "#092947"
        },
        green: {
          100: "#EEFFFD",
          200: "#D3FFFA",
          300: "#B9FFF7",
          400: "#85FFF1",
          500: "#50FFEB",
          600: "#48E6D4",
          700: "#30998D",
          800: "#24736A",
          900: "#184D47"
        }
      },
      maxWidth: {
        none: "none",
        "7xl": "80rem",
        "8xl": "88rem"
      },
      spacing: {
        "7": "1.75rem",
        "9": "2.25rem"
      },
      height: {
        "screen-2/3": "66vh"
      }
    }
  },
  variants: {
    borderRadius: ["responsive", "focus"],
    borderWidth: ["responsive", "active", "focus"],
    width: ["responsive", "focus"],
    backgroundColor: ["even"]
  },
  plugins: [
    function({ addUtilities }) {
      const newUtilities = {
        ".transition-fast": {
          transition: "all .2s ease-out"
        },
        ".transition": {
          transition: "all .5s ease-out"
        }
      };
      addUtilities(newUtilities);
    }
  ]
};
