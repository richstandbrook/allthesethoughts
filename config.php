<?php

use Carbon\Carbon;

return [
    'baseUrl' => 'http://localhost:3000/',
    'production' => false,
    'siteName' => 'All These Thoughts',
    'siteDescription' => 'I have all these thoughts and I\'m pretty sure they all contradict each other',
    'siteAuthor' => 'Rich Standbrook',

    // collections
    'collections' => [
        'posts' => [
            'author' => 'Rich Standbrook', // Default author, if not provided in a post
            'sort' => '-date',
            'path' => function($page) {
                $postName = explode('-', $page->getFilename());
                $date = array_shift($postName) .'/'. array_shift($postName) .'/'. array_shift($postName);
                $slug = str_slug(implode('-', $postName));
  
                return "{$date}/{$slug}";
            },
        ],
        'categories' => [
            'path' => '/categories/{filename}',
            'posts' => function ($page, $allPosts) {
                return $allPosts->filter(function ($post) use ($page) {
                    return $post->categories ? in_array($page->getFilename(), $post->categories, true) : false;
                });
            },
        ],
    ],

    // helpers
    'age' => function ($page) {
        return Carbon::createFromTimestamp($page->date)->age;
    },
    'getDate' => function ($page) {
        return Datetime::createFromFormat('U', $page->date);
    },
    'getExcerpt' => function ($page, $length = 255) {
        $content = $page->excerpt ?? $page->getContent();
        $cleaned = strip_tags(
            preg_replace(['/<pre>[\w\W]*?<\/pre>/', '/<h\d>[\w\W]*?<\/h\d>/'], '', $content),
            '<code>'
        );

        $truncated = substr($cleaned, 0, $length);

        if (substr_count($truncated, '<code>') > substr_count($truncated, '</code>')) {
            $truncated .= '</code>';
        }

        return strlen($cleaned) > $length
            ? preg_replace('/\s+?(\S+)?$/', '', $truncated) . '...'
            : $cleaned;
    },
    'isActive' => function ($page, $path) {
        return ends_with(trimPath($page->getPath()), trimPath($path));
    },
];
