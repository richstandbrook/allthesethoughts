---
extends: _layouts.post
title: Absolute zero downtime deployment
categories: [development, production]
# @see http://blog.moertel.com/posts/2005-08-22-how-to-change-symlinks-atomically.html
date: 2015-04-10
---

So, a common technique to deploy a new release of your app might use a directory structure something like this:

```
 - app/
  - current/ -> releases/2015-01-01-1346/
  - releases/
    - 2015-01-01-1023/
    - 2015-04-10-1346/
```

Then, when you want to deploy a new version of your app or site you clone the source into `app/releases/` and run whatever build and configuration processes you need (`composer install` for example) and then `ln -snf current releases/2015-04-10-1346` and your new release is deployed:

```
 - app/
  - current/ -> releases/2015-04-10-1346/
  - releases/
  …
```

However, did you know that `ln` is not an atomic operation (it doesn’t occur instantaneously). What actually happens is `current/` is _unlinked_ then a new symlink is created in it’s place—this leaves a small window where there is no `current/` directory.

## An atomic solution

Instead use `mv` which is atomic. Create a new symlink `ln -s releases/2015-04-10-1346/ latest` then _move_ it over the top `mv -Tf latest current` and your done—absolute zero downtime.
