---
extends: _layouts.post
title: Proud to be British
categories: [life]
category: life
date: 2016-06-23
---

Seeing [this video of European cities displaying the Union Jack](http://uk.businessinsider.com/union-jack-flag-remain-europe-brexit-referendum-eu-2016-6) has really moved me to post a comment on this controversial topic I was mostly staying away from.

I feel incredibly proud to be British. I don't think our country is perfect, but in recent weeks I've seen how much of the world do think a lot of us. We must be doing something right that people from all over the world would want to come and live here, would want us to stand by them. We are not a huge empire anymore, but as a small nation we have a huge influence in the world.

So rather than selfishly grabbing at sovereignty and shutting ourselves off why don't we stand tall use our influence responsibly and honour the respect the world has in us. I don't believe being in or out of the EU will solve our problems, WE need to be the answer. Pick up some litter down your street, smile at your neighbor's, stand up for injustice, work hard, be kind and use your vote when it's called on rather than sitting by complaining.

Let's give the world a reason to continue to respect this country! Let's show the world what it means to be British. That's what will make Britain great.