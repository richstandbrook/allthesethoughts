---
extends: _layouts.post
title: Building things
categories: [development, businsess]
date: 2015-07-30
---

I decided last night that I am just going to get something done. That's it … I decided. Of course I didn't actualy do anything—except lay awake for half the night thinking about what I could do, and sneezing.

Now I have a cold and a bad nights sleep, but I still feel determined to take the advice of all those blogs and podcasts I keep consuming and just build something.

I have a bunch of ideas (nine at last count) for businesses or products I could build. Whether any are viable, practicle, profitable or will ever see the light of day remains to be seen.

My thanks to [Jonnie Hallman](https://twitter.com/destroytoday) for his work on [cushionapp.com](http://cushionapp.com) where he includes a [journal of his development](http://cushionapp.com/journal) and a comprehenstive [expenses page](http://cushionapp.com/expenses). His journey, openness and end product inspired me to revisit some of my ideas. Hopefuly will be making use of [cushionapp.com](http://cushionapp.com) for my daily work.
