---
extends: _layouts.post
title: X-Clacks-Overhead "GNU Terry Pratchett";
categories: [life]
cover: /assets/images/posts/terry-pratchett.jpg
date: 2015-03-12
---

> AT LAST, SIR TERRY, WE MUST WALK TOGETHER

> Terry took Death’s arm and followed him through the doors and on to the black desert under the endless night.

> The End.

([Via twitter](https://twitter.com/terryandrob/status/576036599047258112))

Really like this idea that came up on [reddit](http://www.reddit.com/r/discworld/comments/2yt9j6/gnu_terry_pratchett/) of keeping Terry Prattchet traveling in the overhead of the internet.

Farewell Sir Terry Prattchet.
