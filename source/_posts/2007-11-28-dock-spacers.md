---
extends: _layouts.post
title: Dock spacers
categories: [osx]
date: 2007-11-28
---

Check this out, want to bring a little organisation to your Dock? Add spacers with this command:

```
defaults write com.apple.dock persistent-apps -array-add '{"tile-type"="spacer-tile";}'
```

Do this as many times as you want spacers then restart the Dock

```
killall Dock
```

Move the spacers around like any other Dock icon, menu click or drag off to remove them. Easy!

Got it here: [http://www.flickr.com/photos/hetima/1839756788/](http://www.flickr.com/photos/hetima/1839756788/)

