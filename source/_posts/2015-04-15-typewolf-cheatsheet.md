---
extends: _layouts.post
title: Typewolf Cheatsheet
categories: [design, typography]
date: 2015-04-15
---

Quotes & apostrophes, dashes & hyphens, useful typographic characters and accents are all covered in  [@typewolf](https://twitter.com/typewolf) 's [Typography Cheatsheet](http://www.typewolf.com/cheatsheet)—along with answers to questions such as:

> What’s the difference between an apostrophe and a closing single quote?

and

> What is the hedera symbol and how do I type it on a Mac?

Take a [look at the cheatsheet](http://www.typewolf.com/cheatsheet) and his other great resources.
