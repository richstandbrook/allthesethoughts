---
extends: _layouts.post
title: Procreate Animate
categories: [animation]
cover: /assets/images/posts/procreate-animation-egg.gif
date: 2019-12-13
featured: true
---


Rather excited by the latest update to [Procreate](https://procreate.art)! Although I'd still like to maintain my love of traditional media (pencils and paints) I've really been enjoying drawing on my iPad since getting the Apple Pencil and [PaperLike screen protector](https://paperlike.com), now I can try my hand at a bit of animation!

There's a whole lot of ways to do animation, I've done a little UI animation in the browser (like on my new [about me page](/richstandbrook.html)). Then there's [Adobe Animate](https://www.adobe.com/uk/products/animate.html) (successor to Flash) which I've yet to invest much time into. I've also done a bit of motion graphics over the years in Apple Motion, Adobe After Effects, and [DaVinci Resolve](https://www.blackmagicdesign.com/products/davinciresolve/). Procreate 5 however, makes it so easy to do simply frame-by-frame animation of hand-drawings – and it's a lot of fun.

Some day I would love to share some of my hand lettering and other illustrations, but at the moment it is all copies of other people's work—I need to work on some original ideas!

<video autoplay loop controls class="sm:w-2/3 mx-auto">
    <source src="/assets/images/posts/procreate-animation-egg.mp4"
            type="video/mp4">

    Sorry, your browser doesn't support embedded videos. 
    <a href="/assets/images/posts/procreate-animation-egg.mp4">Click here to watch it</a>
</video>

This was inspired by a still drawing I saw on Pinterest of an egg sneezing out a chick. I wasn't able to find it again, so if it was yours get in touch!

---

Also, this post marks the updated design of this website. I hope to post more too, but how many times have I said that!

Peace ✌️
