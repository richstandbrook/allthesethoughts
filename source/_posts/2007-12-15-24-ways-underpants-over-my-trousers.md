---
extends: _layouts.post
title: 24 Ways—Underpants over my trousers
categories: [development]
date: 2007-12-15
---

Good to see [24 Ways](http://24ways.org/) back for their third year. Andy Clarke of [Stuff and Nonesense](http://www.stuffandnonsense.co.uk/) has written an article called Underpants Over My Trousers, where he talks about comic design applied to the web. Panel sizes and use of borders, very inspiring ideas well [worth reading](http://24ways.org/2007/underpants-over-my-trousers).
