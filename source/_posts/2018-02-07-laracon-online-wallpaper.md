---
extends: _layouts.post
title: Laracon Online Wallpaper
categories: [development, laravel]
cover: /assets/images/laraconOnline2018/LaraconOnline-gradient.png
date: 2018-02-07
---


I'm really enjoying having simple gradient wallpapers at the moment and the gradient on the [laracon.net](https://laracon.net) website caught my eye.

I made a few versions, the [simple gradient](/assets/images/laraconOnline2018/LaraconOnline-gradient.png), [with the LaraconOnline logo](/assets/images/laraconOnline2018/LaraconOnline-logo.png) and one [with the background artwork](/assets/images/laraconOnline2018/LaraconOnline.png) as well. Enjoy.

Come and say hi in the chat.

<hr class="border-t">

If you're not signed up, It's only $25 [what are you waiting for](https://laracon.net/pay).

![LaraconOnline](/assets/images/laraconOnline2018/LaraconOnline.png)