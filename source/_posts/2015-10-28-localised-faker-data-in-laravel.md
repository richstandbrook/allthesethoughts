---
extends: _layouts.post
title: British Faker data in Laravel model factories
categories: [development]
date: 2015-10-28
---

If you use Laravel you should be using Model Factories. If you’re then check out this [introduction to Model Factories on Laravel News](https://laravel-news.com/2015/10/learn-to-use-model-factories-in-laravel-5-1/).

The problem I’ve struggled with for a while is by default the instance of [Faker](https://github.com/fzaninotto/Faker) is set to use `en_US`. This is’t often a problem, until you start generating phone numbers and postal addresses.

Luckily there is an easy way to fix this—thanks to dependency injection. Open up `app/Providers/AppServiceProvider.php` and within the `register` method add this:

```php
$this->app->singleton(FakerGenerator::class, function () {
  return FakerFactory::create('en_GB');
});
```

Now the Faker instance injected into the Model Factories will be localised—you can obviously change the locale to whatever you need. You can see the [available Faker locales here](https://github.com/fzaninotto/Faker/tree/master/src/Faker/Provider).
