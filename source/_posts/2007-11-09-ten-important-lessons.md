---
extends: _layouts.post
title: 10 important lessons
categories: [life]
date: 2007-11-09
---

[Mark Baterson](http://www.markbatterson.com), Pastor of [National Comminity Church](http://theaterchurch.com) in the States, posted a list of “[The Ten Most Important Lessons I’ve Learned Over the Past Ten Years](http://www.markbatterson.com/uncategorized/10-things-ive-learned-in-10-years/)”. I rather like them:

 1. Pray ridiculous prayers
 1. Be Yourself
 1. Put Your Family First
 1. Change of Pace + Change of Place = Change of Perspective
 1. Leaders are Readers
 1. Everything is an Experiment
 1. 1% of What You Do Makes 99% of the Difference
 1. Church is a tag-team sport
 1. The Most Important and Most Difficult Job of a leader is creating culture
 1. Market Internally
