---
extends: _layouts.post
title: The seven rules of Unobtrusive JavaScript
categories: [development, javascript]
date: 2007-11-16
---

Chris Heilmann, over at [wait-till-i.com](http://wait-till-i.com) is doing a workshop at the Paris Web Conference on unobtrusive JavaScript and covering his “Seven rules of Unobtrusive JavaScript”

 1. Do not make any assumptions
 1. Find your hooks and relationships
 1. Leave traversing to the experts
 1. Understand browsers and users
 1. Understand Events
 1. Play well with others
 1. Work for the next developer

Read more in his [article here](http://icant.co.uk/articles/seven-rules-of-unobtrusive-javascript/).
