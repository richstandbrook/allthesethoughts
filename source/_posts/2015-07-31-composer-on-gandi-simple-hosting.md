---
extends: _layouts.post
title: Using composer on Gandi Simple Hosting
categories: [development, production]
date: 2015-07-31
---

_Update: Good news, **you can now use composer natively on Gandi Simple Hosting** with thier [update to PHP 5.6](https://www.gandi.net/news/en/2016-02-08/6962-php_5.6_now_available_on_simple_hosting)._

__TL;DR__ [Install composer](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx) in `~/web/includes` and set the [config option `cache-dir`](https://getcomposer.org/doc/06-config.md#cache-dir) to `~/tmp/composer` in your `composer.json`. Change to your `htdocs` directory and run `~/web/includes/composer install`.

---

> However, not having to think is sometimes a bonus

When it comes to web hosting I'm a fan of [Digital Ocean](https://www.digitalocean.com/?refcode=00958c500705), price, quality, support, community. All second to none.

However, not having to think is sometimes a bonus and for me that's where [Gandi Simple Hosting](https://www.gandi.net/hosting/simple) comes in. 

Simply choose the configuration you want; PHP, Node.js, or Python with the desired database MySQL, PostgreSQL or mongoDB. You get a container in the Gandi cloud with the environment all ready to go. Sites (apache vhosts) can be added in the Gandi services panel, but from here you're free to do as you please.

Great. Now I have a PHP/PostgreSQL instance, turn on SSH and I can login and `git clone` my repo fantastic, no … wait, how to install the dependancies? If you've been developing with PHP recently then you probably are making use of [composer](https://getcomposer.org), unfortunately Gandi doesn't come with `composer` pre-installed, luckily it's pretty simple and there is just one gotcha I came across.

SSH into your instance, and `cd  ~/web/includes` and install composer (you could put it anywhere writable but `~/web/includes` was the most commonly accessible place I could find):

``` bash
curl -sS https://getcomposer.org/installer | php
```

Next `cd ~/web/vhosts/example.com/htdocs` edit your `composer.json` because here's the gotcha: set the [config option `cache-dir`](https://getcomposer.org/doc/06-config.md#cache-dir) to somewhere writable, `~/tmp/composer` for instance.

``` json
{
  "config": {
    "cache-dir": "~/tmp/composer"
  }
}
```

That's it. `~/web/includes/composer install`.

## Epilogue

It's worth noting that in a lot of cases frameworks such as Laravel have a `/public` directory—this is good practice and keeps config files out of the public server. In this instance you should place your code in it's own directory next to `htdocs`. For instance, ` ~/web/vhosts/example.com/my-app` the you should make `htdocs` a symbolic link to `my-app/public`. Take a look at my post on [Absolute zero downtime deployment](/2015/04/10/absolute-zero-downtime-deploy.html) for an expansion on this idea.
