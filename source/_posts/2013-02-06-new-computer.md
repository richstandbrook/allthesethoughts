---
extends: _layouts.post
title: Simple, approachable, wonderful!
description: Something about the physical feedback of this thing I just love.
tags : [technology]
cover: /assets/images/posts/macintosh-se.jpg
date: 2013-02-06
---

1988\. Apple have already been taking over the world with the Apple II. This little machine that I got my hands on is a revision or two from the ground breaking, industry defining Macintosh that [introduced itself just four years before](http://www.youtube.com/watch?v=2B-XwPjn9YY).

My own little piece of history that embodys everything that Apple have always stood for. Simple, approachable, usable. The grab handle and carry case made this little machine feel like something you could take on holiday, a single button on the mouse leaves nothing to question, a smiling Macintosh icon during boot reassures you everything is going to plan. Never before had computers been something *you* could own in the same way.

What really blew me away about this thing was it’s speed. Yes I did say that. Take a look at this [video comparing a 2007 PC with the 1984 Macintosh](http://www.youtube.com/watch?v=tmRJ649ICPU) the mac is ready in less than twenty seconds, the PC more than two minutes!

Here is a little video of my new Macintosh SE. It has Superdraw and Microsoft Word and Excel, with a postscript printer I could start knocking out some awesome posters for my friends band!

<iframe src="https://player.vimeo.com/video/59083188?color=ffffff&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

I find it easy to forget how far technology has come in my lifetime. It’s not surprising considering this was filmed in High Definition on my iPhone. But in the early ‘90s I was saving programs in Basic to a cassette tape from my ZX Spectrum 64k. Having this little chap watching over me while I work on my 24" iMac instils a sense of a gravity and respect.

![Macintosh SE and Steve](/assets/images/posts/macintosh-se-steve.jpg)
