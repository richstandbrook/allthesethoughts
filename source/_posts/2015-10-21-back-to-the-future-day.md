---
extends: _layouts.post
title: Happy Back to the Future day!
categories: [life, film]
cover: /assets/images/posts/back-to-the-future.jpg
date: 2015-10-21
---

So October 21, 2015 has arrived. [Flying cars](http://www.aeromobil.com)? [Hoverboards](http://hendohover.com)? Unfortunately we're not quite there yet—however the lack of fax machines is not such a sad thing.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Pl092whRLlI" frameborder="0" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/CsHBKcwyK3I" frameborder="0" allowfullscreen></iframe>
