---
extends: _layouts.post
title: Pecl json extension
categories: [development, php]
date: 2008-03-11
---

This just took me too long to figure out. I needed `json_encode` function, and it seems that the fedora yum repos. doesn’t have a very up-to-date version of php5. I didn’t want to have to go all out and configure and make php from scratch so I looked to pecl for help. After installing pear and much trying I found that I also needed the php-devel package in order to compile pecl packages. Then I also discovered that I need to specificaly enable the extension in the php config. I did it by creating a file in `/etc/php.d` called `json.ini` with the following:

```
; Enable json extension module
extension=json.so
```

Restart apache and your good.
