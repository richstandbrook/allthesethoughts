---
extends: _layouts.post
title: Material Theme for PhpStorm
categories: [development]
date: 2015-10-28
---

I have a bit of a love-hate relationship with Material design. I defiantly think it gets over and incorrectly used. That said I am enjoying the new [Material Theme for JetBrains IDEs](https://github.com/ChrisRM/material-theme-jetbrains) – [PhpStorm](https://www.jetbrains.com/phpstorm/) in my case – by Chris Magnussen. The theme comes in the form of a plugin, once installed you get three syntax colour themes – Light, Regular and Dark – and it also includes new file icons for the Project window.

Well worth a look if you use a JetBrain IDE.

![JetBrains Material Theme](/assets/images/posts/jetbrains-material-theme.png)

Source: [Laravel News](https://laravel-news.com/2015/10/material-theme-for-phpstorm/)
