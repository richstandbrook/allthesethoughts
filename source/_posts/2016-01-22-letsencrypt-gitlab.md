---
extends: _layouts.post
title: Let’s Encrypt Gitlab
categories: [development]
date: 2016-01-22
---

[Let’s Encrypt](https://letsencrypt.org) is now publicly providing free SSL certificates so I thought it would be a good time to get on and secure my [Gitlab](https://gitlab.com) instance.

The usual web search for anyone that may have walked the road before me turned [this up on Stack Overflow](http://stackoverflow.com/questions/34189199/how-do-i-use-let-s-encrypt-with-gitlab) – which was helpful to get me thinking in the right direction, however I though I would document my steps here as I felt it wasn't all that difficult.

_As Hay points out on Stack Overflow you should refer to [the steps on letsencrypt.org](https://letsencrypt.org/howitworks/) as it is still in beta and may change._

## Install Let’s Encrypt
```
$ git clone https://github.com/letsencrypt/letsencrypt
$ cd letsencrypt
```

## Get the certificate

So if you have ever had the joy of applying for SSL certificates before you will understand just how fantastic Let’s Encrypt really is.

```
./letsencrypt-auto certonly --standalone -d gitlab.example.com
```

… that’s it.

Ok so the first time you run Let’s Encrypt you’ll see it do a bunch of stuff as it sets up it’s Python environment and downloads dependancies—don’t worry about any of that. You then get a screen to enter your email address. But really that’s all there is, you have a valid SSL certificate in `/etc/letsencrypt/live/gitlab.example.com/`.

_Minor note; you may well need to stop gitlab, `gitlab-ctl stop` first as Let’s Encrypt complained about not being able to use port 80. Perhaps that was just me?_

## Configure Gitlab

* Open up `/etc/gitlab/gitlab.rb` and make sure `external_url` now references your domain with `https` rather than `http`. I also set `redirect_http_to_https` to true.

* Next update the location of your certificate and private key:
 
```
nginx['ssl_certificate'] = "/etc/letsencrypt/live/gitlab.example.com/fullchain.pem"
nginx['ssl_certificate_key'] = "/etc/letsencrypt/live/gitlab.example.com/privkey.pem"
```
_(Update the location rather than copying the files to Gitlab’s default to make renewing the certificates simpler)_

* Save your changes and run `gitlab-ctl reconfigure`.

And you’re done.

Let’s Encrypt certificates are only valid for 90 days so it's recommended that you set up a script to renew them which they describe on [letsencrypt.org/howitworks](https://letsencrypt.org/howitworks/).
