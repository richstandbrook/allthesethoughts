---
extends: _layouts.post
title: Calling properties dynamically in PHP
categories: [development, php]
date: 2007-11-06
---

I often forget how useful curly brackets can be. For example from within a double quoted string you can do more than basic variable replacement.

``` php
$obj->property = 'property from an object';
print $foo = "This string includes a {$obj->property}.";
```

But a use that is not seen as commonly is the ability to call a property of an object dynamically. I sometimes see this with a single variable:

``` php
$prop_str = 'property';
print $obj->$prop_str;
```

However, this has it’s limits when writing object oriented programmes when the string you want to use as the property name is within another object. You would have to pull it out into a temporary variable:

``` php
$tmp_str = $foo->bar->baz;
print $obj->$tmp_str;
```

With curly brackets you can cut this code down and make it clearer what is going on:

``` php
print $obj->{$foo->bar->baz}
```

And a working example:

``` php
  class foo {

    var $bar;

    function __construct() {
    
      // do something with a database to set $this->bar
      $this->bar = 'foobaz';
    }
  }

  $my_foo = new foo();
  $baz->foobaz = 'I am bar, called dynamically';

  print $baz->{$my_foo->bar};
```
