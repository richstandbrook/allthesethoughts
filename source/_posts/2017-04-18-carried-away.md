---
extends: _layouts.post
title: Carried Away
categories: [life, Raspberry Pi]
cover: /assets/images/posts/carried-away.jpg
date: 2017-04-18
featured: true
---

I’ve been meaning to do some Raspberry Pi projects with my daughter for ages. We decided to get on and try getting a Pi to fly, recording video as it travels up attached to some helium filled balloons. I decided to use one of the recently released [Pi Zero W](https://www.raspberrypi.org/products/pi-zero-w) to keep the weight down, I already had a couple of Pi Camera’s about and we hoped to track and graph altitude with the SenseHat and stream the video to my laptop as it went.

As this last Easter holiday approached I was keen to just get on and at least get a prototype launched without getting carried away with fancy tracking and streaming. So we chose to forgo the SenseHat and just hit record and send it up. This seemed to work well … to begin with.

![Pi to fly](/assets/images/posts/pi-to-fly.jpg)


We filled about 30 balloons before the Pi Zero and battery lifted off the ground. Tethered to a kite string on a real the Pi started it’s accent. After a while of fighting winds we gradually pulled it back but encouraged by the kids to go higher—and my own desire to capture some more footage—we sent it up again. It traveled straight up without much spinning and I was excited that we would get a good video. As we started to gently pull it back down to earth it dropped past a fence, which I think may have been the problem. The thin kite string pulled against the fence panels, frayed and the balloons—stirred by their sense of freedom—ascended again and was carried away into the distance.

![Pi in the Sky](/assets/images/posts/pi-in-the-sky.jpg)

I [tweeted of our sad tail](https://twitter.com/richstandbrook/status/854010448479485952) and so far it’s had over 8,000 impressions. Whether it will result in our little black box finding it’s way home, well I think the chances are small. Stranger things have happened.
