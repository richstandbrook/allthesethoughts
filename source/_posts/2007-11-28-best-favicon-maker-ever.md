---
extends: _layouts.post
title: Best favicon maker ever!
categories: [design, development, tools]
date: 2007-11-28
---
[favikon.com](http://favikon.com) is so simple and intuitive. A useful little tool.
