---
extends: _layouts.post
title: Enormously significant insignificance
categories: [life, the universe, everything]
cover: /assets/images/posts/pale-blue-dot.jpg
date: 2015-04-29
---

Did you see it? Take another look at the [cover image](/assets/images/posts/pale-blue-dot.jpg).

On 14 February, 1990 the Voyager 1 spacecraft—from beyond Neptune, 3,718,232,290 miles away—looked back at our solar system and took pictures of the planets. The cover image of this post shows the photo of earth. Look close at the right hand light artifact, just over halfway down is a pale blue dot. That’s us, you and me. It’s the place where the entire human race lives out it’s existance.

You may have seen this image before but I hadn’t come accross it until last night when I watched [Indescribable by Louie Giglio](https://www.youtube.com/watch?v=oNgJhuICrVA). It really made me consider how unfathomably vast our universe is. We use the phrase “known universe” to describe what we have seen and studied … so far. It seems that however big we build our telescopes or however far we send spacecrafts we keep finding more and I can’t help but imagion God longing for us to see further and find out how the universe just keeps on going.

I’m humbled by the contrast of the image of the Pale Blue Dot against the idea that the creator of this vast universe and the ranging fury of burning stars cares about you and me—cares enough to come to our tiny Pale Blue Dot to die for us. That’s the enormous significance of our insignificance.

Take 40 minutes to watch [Indescribable by Louie Giglio](https://www.youtube.com/watch?v=oNgJhuICrVA). It might just give you a new perspective.

As it happens the Pale Blue Dot images [turned 25 this year](http://voyager.jpl.nasa.gov/news/pale_blue_25.html).

_Update: [My brother](http://markstandbrook.com) pointed me at a couple of nice videos of Carl Sagan reading his own words on the Pale Blue Dot. Firstly [a very cool animation by Joel Sumerfield](https://vimeo.com/51960515) and then [the complete reading](https://www.youtube.com/watch?v=4PN5JJDh78I)—which is worth watching full screen._
