---
extends: _layouts.post
title: Codeigniter 1.6.0
categories: [development, php]
date: 2008-01-31
---

Lightweight PHP framework, Codeigniter releases 1.6. This release includes:

 * A Database Forge class: database agnostic table and field manipulation
 * An enhanced Active Record library
 * A revised Session Class with “flash variables” and additional security
 * An extendable Model Class with auto-loadable Models
 * Extendable Helpers, and additions and enhancements to existing Helpers
 * A revised view architecture that allows for simple inclusion of multiple views
 * And over 120 different enhancements, improvements and bug fixes

[http://codeigniter.com/news/codeigniter_1_6_hits_the_streets/](http://codeigniter.com/news/codeigniter_1_6_hits_the_streets/)
