<nav class="hidden lg:flex md:pt-2 items-center justify-end text-lg">
    <a title="{{ $page->siteName }}" href="/thoughts"
        class="ml-6 text-white border-b-4 border-transparent hover:border-blue-500 {{ $page->isActive('/thoughts') ? 'border-blue-500' : '' }}">
        All the thoughts
    </a>

    <a title="About Rich Standbrook" href="/richstandbrook"
        class="ml-6 text-white border-b-4 border-transparent hover:border-red-500 {{ $page->isActive('/richstandbrook') ? 'border-red-500' : '' }}">
        Me
    </a>
</nav>
