<nav id="js-nav-menu" class="fixed inset-0 bg-purple-500 p-6 pt-24 z-20 hidden lg:hidden">

    <a title="{{ $page->siteName }}" href="/thoughts"
       class="nav-menu__item hover:border-blue-500 {{ $page->isActive('/thoughts') ? 'border-blue-500' : '' }}">
        All the thoughts
    </a>

    <a title="About Rich Standbrook" href="/richstandbrook"
       class="nav-menu__item hover:border-red-500 {{ $page->isActive('/richstandbrook') ? 'border-red-500' : '' }}">
        About Rich
    </a>
</nav>
