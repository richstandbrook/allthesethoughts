@extends('_layouts.master')

@section('body')
    @if ($featuredPost = $posts->where('featured', true)->first())
        <header class="flex items-center justify-center pt-24 px-3 w-full min-h-screen bg-purple-500 bg-cover bg-center bg-darken text-white"
        @if ($featuredPost->cover) style="background-image: url('{{ $featuredPost->cover }}')" @endif>
            <figure class="absolute z-0 inset-0 bg-cover bg-bottom"
                    style="background-image: url(assets/images/background.svg)"></figure>

            <article class="relative z-10 max-w-xl text-center rellax overflow-hidden sm:overflow-visible"  data-rellax-speed="7">
                @include('_components.line', ['class' => 'absolute right-0 w-12 -mr-8 sm:-mr-10 text-red-500 rellax', 'rellax-speed' => '4'])

                <h2 class="text-3xl sm:text-5xl leading-tight">
                    <a href="{{ $featuredPost->getUrl() }}" title="Read {{ $featuredPost->title }}"
                       class="text-white font-extrabold">
                        {{ $featuredPost->title }}
                    </a>
                </h2>

                <p class="font-medium text-purple-200">
                    {{ $featuredPost->getDate()->format('F j, Y') }}
                </p>

                <p class="text-xl sm:text-2xl">
                    {!! $featuredPost->getExcerpt() !!}
                    <a href="{{ $featuredPost->getUrl() }}" title="Read - {{ $featuredPost->title }}"
                       class="text-blue-500 font-bold tracking-wide whitespace-no-wrap">
                        Read more
                    </a>
                </p>
            </article>

            @include('_components.joined-lines', ['class' => 'absolute inset-x-0 w-32 mt-48 -ml-24 sm:-ml-8 text-green-500 rellax', 'relax-speed' => 10])
        </header>
    @endif

    <div class="mb-10">
        @include('_components.newsletter-signup')
    </div>

    @foreach ($posts->where('featured', false)->take(6)->chunk(2) as $row)
        <div class="flex flex-col md:flex-row max-w-5xl mx-auto">
            @foreach ($row as $post)
                <div class="w-full">
                    @include('_components.post-preview-inline')
                </div>
            @endforeach
        </div>
    @endforeach
@stop
