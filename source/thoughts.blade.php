---
pagination:
    collection: posts
    perPage: 5
---
@extends('_layouts.master')

@push('meta')
    <meta property="og:title" content="{{ $page->siteName }} Blog" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="The list of blog posts for {{ $page->siteName }}" />
@endpush

@section('body')

    <section class="mt-24 max-w-xl mx-auto">
        @foreach ($pagination->items as $post)
            @include('_components.post-preview-inline')
        @endforeach
    </section>

    @if ($pagination->pages->count() > 1)
        <nav class="flex text-base justify-center my-8">
            @if ($previous = $pagination->previous)
                <a
                    href="{{ $previous }}"
                    title="Previous Page"
                    class="bg-purple-500 hover:bg-purple-600 hover:text-purple-100 rounded mr-3 px-4 py-2 text-purple-200"
                >&LeftArrow;</a>
            @endif

            @foreach ($pagination->pages as $pageNumber => $path)
                <a
                    href="{{ $path }}"
                    title="Go to Page {{ $pageNumber }}"
                    class="bg-purple-500 hover:bg-purple-600 hover:text-purple-100 rounded mr-3 px-4 py-2 {{ $pagination->currentPage == $pageNumber ? 'font-bold text-white' : 'text-purple-200' }}"
                >{{ $pageNumber }}</a>
            @endforeach

            @if ($next = $pagination->next)
                <a
                    href="{{ $next }}"
                    title="Next Page"
                    class="bg-purple-500 hover:bg-purple-600 hover:text-purple-100 rounded mr-3 px-4 py-2 text-purple-200"
                >&RightArrow;</a>
            @endif
        </nav>
    @endif
@stop
