@extends('_layouts.master')

@push('meta')
    <meta property="og:title" content="About {{ $page->siteName }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="A little bit about {{ $page->siteName }}"/>
@endpush

@section('body')
    <section class="mt-48 mb-16 p-4 max-w-5xl mx-auto text-purple-100 text-4xl md:text-6xl leading-none">

        Hi there, my name's
        <b class="text-white whitespace-no-wrap">Rich Standbrook</b>
        <b class="text-red-500">I'm a creative design &amp; development consultant</b>
        <b class="text-green-500">based in Peterborough, UK</b>

    </section>

    <section id="attributes"
             class="hidden flex justify-around lg:justify-between flex-wrap lg:flex-no-wrap text-white twist my-8 max-w-2xl lg:max-w-full">
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-yellow-500 text-yellow-900">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="h-full fill-current">
                    <path d="M10.9 76.31l-2.36 2.36c-1.26 1.25-1.95 2.92-1.95 4.7s.7 3.44 1.94 4.68a6.63 6.63 0 0 0 9.38 0l.47-.46a6.56 6.56 0 0 0 6.58 5.9c1.78 0 3.44-.69 4.7-1.93l5.2-5.21a6.7 6.7 0 0 0 6.68 5.91c1.7 0 3.3-.65 4.48-1.83l7.1-7.1 5.06 5.05a18.14 18.14 0 0 0 7.25 4.72 8.33 8.33 0 0 0 8.4-2.02l12.03-11.95c4.52-4.51-.86-14.9-2.62-17.98l-.02-.02v-.03l-3.32-5.28a25.43 25.43 0 0 0 2.27-3.94 13.96 13.96 0 0 0 1.03-8.3l10.52-10.52a4.37 4.37 0 0 0 0-6.18l-19.1-19.1a4.37 4.37 0 0 0-6.17 0l-8.83 8.83c-7.44-4.32-21.53 3-25.94 5.51l-.01.01-.03.01-14.66 9.2c-1.1.54-1.79 1.25-2.07 2.12-.45 1.36.24 2.65.75 3.58l.23.45.42.85a6.32 6.32 0 0 0 4.2 3.27 7.65 7.65 0 0 0 5.3-.43l5.2-2.48L6.95 65.03a6.64 6.64 0 0 0 3.95 11.28zM27.87 89.8a4.12 4.12 0 1 1-5.85-5.85l16.22-16.21c.21.65.55 1.26 1.06 1.78l4.43 4.43-7.1 7.1h-.01l-8.75 8.75zm16.37-1.13a3.87 3.87 0 0 1-2.8 1.1 4.28 4.28 0 0 1-4.15-4.23c0-1.05.4-2.01 1.1-2.72l7.1-7.1 5.86 5.84-7.11 7.11zm39.85-11.3L72.06 89.3a5.79 5.79 0 0 1-5.86 1.42 15.67 15.67 0 0 1-6.24-4.1l-5.93-5.93s0-.02-.02-.03l-.03-.02-12.9-12.9a2.1 2.1 0 0 1-.1-2.76l.4-.38.04-.05.03-.02c.78-.55 1.97-.53 2.65.13l.09.07L55.53 76.1l.21.2a1.24 1.24 0 0 0 1.77 0c.49-.48.49-1.27 0-1.76L45.9 62.9a1.07 1.07 0 0 0-.1-.08l-5.44-5.45a2.15 2.15 0 1 1 3.05-3.05L59.5 70.4l.35.35a1.25 1.25 0 0 0 1.79-1.74l-.34-.34s0-.02-.02-.02L45.3 52.66l-.11-.1-2.61-2.62a2.15 2.15 0 1 1 3.06-3.04L63.9 65.17a1.25 1.25 0 0 0 1.77-1.76L51.2 48.94c-.4-.4-.62-.94-.62-1.53a2.13 2.13 0 0 1 2.13-2.14c.59 0 1.13.22 1.54.63L74 65.47l.04.03a1.25 1.25 0 0 0 1.51.31c.62-.3.87-1.06.56-1.68l-1.57-3.3-.01-.03-1.9-4.02c-.5-1.06-.23-2.79.83-3.3l.88-.44c.18-.1.5-.28.68-.32.03.04.1.13.16.27l.07.13 2.02 3.23.06.1 3.74 5.95c3.9 6.85 5.12 12.86 3.02 14.96zM70.22 9.56a1.9 1.9 0 0 1 2.64 0l19.1 19.09c.72.73.72 1.91 0 2.64l-9.55 9.55a14.23 14.23 0 0 0-3.12-4.79L61.58 18.2l8.64-8.64zM8.7 66.78l30-30.3c2.2 2.23 5.7 4.33 11.13 4.33 1 0 2.08-.08 3.21-.23a17.84 17.84 0 0 0 7.4-2.69 1.25 1.25 0 0 0-1.38-2.08 15.35 15.35 0 0 1-6.36 2.29c-5.35.74-9.67-.58-12.56-3.78a1.21 1.21 0 0 0-.1-.36 1.25 1.25 0 0 0-1.66-.59L26.73 38.9c-1.04.5-2.38.6-3.6.28a3.8 3.8 0 0 1-2.58-1.94l-.43-.87-.27-.52c-.3-.54-.69-1.28-.58-1.63.07-.2.37-.44.84-.66l.12-.07 14.68-9.2c8.59-4.9 20.17-9.06 24.56-4.68l18.04 18.2a11.68 11.68 0 0 1 2.38 13.01c-.42.92-.9 1.8-1.45 2.66l-1.03-1.65c-.4-.79-.94-1.3-1.6-1.52-1.07-.34-2.03.18-2.66.53l-.77.38c-2.4 1.15-3.04 4.45-2 6.63l.44.93-14.8-14.66a4.63 4.63 0 0 0-3.3-1.37h-.01a4.6 4.6 0 0 0-4.41 3.29l-.92-.92a4.69 4.69 0 0 0-6.58 0 4.63 4.63 0 0 0-.25 6.29 4.6 4.6 0 0 0-1.97 7.72l2.35 2.35-1.22 1.22a3.63 3.63 0 0 0-.54.54l-.04.04v.01l-.1.1v.01L16.14 86.28a4.23 4.23 0 0 1-5.85 0c-.78-.77-1.2-1.8-1.2-2.92s.42-2.14 1.2-2.92l7.8-7.8 21.07-21.07a1.25 1.25 0 0 0 0-1.78 1.25 1.25 0 0 0-1.77 0L15.8 71.4l-1.25 1.24a4.14 4.14 0 0 1-5.84-5.86z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Husband &amp; father</p>
        </article>
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-red-500">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="h-full fill-current">
                    <path d="M13.46 89.54a.96.96 0 0 1-.96-.96V11.42c0-4.7 3.83-8.54 8.54-8.54h65.5c.53 0 .96.43.96.96V81c0 .53-.43.96-.96.96h-65.5c-4.64 0-6.62 1.98-6.62 6.62 0 .53-.43.96-.96.96zM21.04 4.8a6.62 6.62 0 0 0-6.62 6.61v71c1.42-1.58 3.63-2.38 6.62-2.38h64.54V4.8z"></path>
                    <path d="M86.54 97.12h-65.5a8.55 8.55 0 0 1 0-17.08h65.5c.53 0 .96.43.96.96v15.15c0 .53-.43.97-.96.97zm-65.5-15.16a6.63 6.63 0 0 0 0 13.23h64.54V81.96zM50 61.33a.96.96 0 0 1-.96-.96v-35.9a.96.96 0 1 1 1.92 0v35.9c0 .53-.43.96-.96.96z"></path>
                    <path d="M60.74 34.93H39.26a.96.96 0 1 1 0-1.92h21.47a.96.96 0 0 1 0 1.92zM79.4 89.54H27a.96.96 0 1 1 0-1.92h52.4a.96.96 0 1 1 0 1.92z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Jesus follower</p>
        </article>
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-green-500 text-green-900">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="h-16 fill-current">
                    <path fill-rule="nonzero"
                          d="M15.44 69.18c0 14.49 7.96 15.59 10.4 15.59.46 0 .7-.08.7-.08v-3.3h-.47c-1.76 0-7.34-.86-7.34-12.12v-8.99c0-8.55-6.04-10.76-6.04-10.76v-.16s6.04-2.55 6.04-10.76v-7.88c0-11.27 5.58-12.12 7.34-12.12h.46v-3.3h-.69c-2.44 0-10.4 1.1-10.4 15.5v6.86c0 9.07-6.5 10.09-6.5 10.09v3.39s6.5 1.01 6.5 10.08v7.96zM75.03 81.38v3.3s.23.09.69.09c2.45 0 10.4-1.1 10.4-15.59v-7.96c0-9.07 6.5-10.08 6.5-10.08v-3.4s-6.5-1.01-6.5-10.08V30.8c0-14.4-7.95-15.5-10.4-15.5h-.7v3.3h.47c1.76 0 7.34.85 7.34 12.12v7.88c0 8.21 6.04 10.76 6.04 10.76v.17s-6.04 2.2-6.04 10.76v8.98c0 11.26-5.58 12.11-7.34 12.11h-.46zM69.03 72.84a6.43 6.43 0 0 0-5-6.25V33.47a6.42 6.42 0 1 0-2.84 0v21.49c-.4-.27-.8-.5-1.24-.66l-19.3-7.51a3.54 3.54 0 0 1-2.27-3.32v-10a6.42 6.42 0 1 0-2.85 0v10a6.38 6.38 0 0 0 4.09 5.98l19.3 7.5a3.54 3.54 0 0 1 2.27 3.33v6.31a6.42 6.42 0 1 0 7.84 6.25zm-9.98-45.62a3.57 3.57 0 1 1 7.13 0 3.57 3.57 0 0 1-7.13 0zm-25.66 0a3.57 3.57 0 1 1 7.13 0 3.57 3.57 0 0 1-7.13 0zm25.66 45.62a3.57 3.57 0 1 1 7.14 0 3.57 3.57 0 0 1-7.14 0z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Developer</p>
        </article>
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-blue-500 text-blue-100">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="h-16 fill-current">
                    <path d="M52.86 42.87l13.32-13.32 2.27 2.27-13.32 13.32z"></path>
                    <path d="M63.02 18.08L41.64 39.46l-.37-.07-4.16 21.5 21.43-4.52 21.38-21.39-16.9-16.9zM41.17 56.84l.83-4.3 3.45 3.47-4.28.83zm8.09-1.57l-6.53-6.52.93-4.78 10.37 10.38-4.77.92zm8.04-2.2L44.93 40.72l18.09-18.1 12.37 12.37-18.1 18.1zM87.9 10.1c-2.25-2.26-5.25-3.5-8.44-3.5s-6.2 1.24-8.45 3.5l-2.7 2.69 16.9 16.9 2.7-2.7a11.87 11.87 0 0 0 3.5-8.44c0-3.2-1.24-6.2-3.5-8.46zm-2.26 14.63l-.42.42L72.85 12.8l.42-.43c1.66-1.65 3.85-2.56 6.19-2.56s4.53.91 6.18 2.56c1.65 1.65 2.56 3.85 2.56 6.19s-.9 4.53-2.56 6.18zM81.02 33.43L64.58 17l2.27-2.27 16.44 16.44zM36.94 72l-.17 2.99c3.8.21 7.02 1.5 9.05 3.6 2.27 2.37 2.89 5.98 1.45 8.4a6.87 6.87 0 0 1-2.47 2.34c-2.35 1.38-5.25 1.45-8.18.21-3.56-1.51-6.41-4.74-7.26-8.22-1.5-6.2 1.46-13.58 7.54-18.82l-1.96-2.27c-6.93 5.97-10.26 14.52-8.5 21.8 1.08 4.43 4.53 8.37 9 10.27 1.75.74 3.53 1.1 5.25 1.1 2 0 3.93-.49 5.62-1.47a9.89 9.89 0 0 0 3.53-3.4c2.15-3.6 1.36-8.65-1.86-12.01-2.55-2.66-6.47-4.27-11.04-4.53zM8.59 83.08l2.9.77c1.13-4.2 5.74-7.34 10.08-6.84l.34-2.98c-5.82-.67-11.8 3.39-13.32 9.05z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Designer</p>
        </article>
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-gray-400 text-gray-800">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" class="h-16 fill-current">
                    <path d="M22.4 47.8c0-.6-.4-1-1-1H4c-.6 0-1 .4-1 1s.4 1 1 1h17.4c.6 0 1-.5 1-1zM8 51.8H6c-.6 0-1 .4-1 1s.4 1 1 1h2c.6 0 1-.4 1-1s-.4-1-1-1zm4.7-9c0-.6-.4-1-1-1H2c-.6 0-1 .4-1 1s.4 1 1 1h9.7c.6 0 1-.5 1-1z"></path>
                    <path d="M62.4 44.5c-.3-.6-.9-1.1-1.5-1.4l-7.6-2.9-3.1-1.1c-.9-.4-1.7-1-2.2-1.8L45.4 33c.5-.1.8-.5.8-1 0-.6-.4-1-1-1h-1L43 29c.5-.1.8-.5.8-1 0-.6-.4-1-1-1h-1l-1-1.6c.4-.1.7-.5.7-.9 0-.6-.4-1-1-1h-.9l-1.3-2.1s0-.1-.1-.1a5.08 5.08 0 0 0-4.5-1.9c-1.5.2-2.7 1.1-3.2 2.2-.1.3-.1.6.1.9l8.3 14.2c.1.2.3.4.5.4L49.6 41h.1l3 1.1 7.5 2.9c.2.1.4.2.5.5.4.8.4 1.6-.1 2.4H44.3c-1 0-2-.3-2.8-.9L13.6 26.3s.4-.2.9-.9.7-1.6.7-2.5l-.9-8.4 1.2-1.9c.3-.4.9-.5 1.3-.2.3.3 1.5 1.3 4.4 6 1.5 2.2 3.9 3.8 6.6 4.2.5.1 1.1-.3 1.2-.8s-.3-1.1-.8-1.2c-2.1-.4-4.1-1.6-5.3-3.4-3.5-5.4-4.5-6.2-4.9-6.5-1.3-.9-3.1-.6-4.1.7l-6.1 9.7c-.4.6-.5 1.3-.4 2-.6.2-1.2.5-1.5 1.1l-1.6 2.5c-.9 1.3-.5 3.1.8 4 .9.7 2.8 1.9 5.6 3.7.2.1.4.2.5.2.3 0 .6-.2.8-.5.3-.5.2-1.1-.3-1.4-2.7-1.8-4.6-3-5.5-3.7-.4-.3-.5-.8-.2-1.3l1.5-2.3c.3-.4.9-.5 1.3-.2l31.4 23.4c1.2.9 2.6 1.3 4 1.3H58c-2.1 1-5 1.9-7.5 1.9h-6.4c-1.7 0-3.4-.5-4.8-1.4-5.8-3.7-16.6-10.6-24.5-15.7-.5-.3-1.1-.2-1.4.3-.3.5-.2 1.1.3 1.4 7.7 5 18.2 11.7 24.1 15.5H12c-.6 0-1 .4-1 1s.4 1 1 1h38.4c4.1 0 10-2.2 11.5-4.3 1.3-1.8 1.4-3.5.5-5.1zm-22.1-9l-7.8-13.3c.3-.3.8-.6 1.4-.7.6-.1 1.7 0 2.7 1.1l.6 1h-.9c-.6 0-1 .4-1 1s.4 1 1 1h2.1l1 1.6h-.8c-.6 0-1 .4-1 1s.4 1 1 1h2l1.2 1.9H41c-.6 0-1 .4-1 1s.4 1 1 1h2l2.8 4.4-5.5-2zM9.5 22.2l3.1-4.9.6 5.8c0 .4-.1.8-.3 1.1s-.6 1.1-1.5.4c0 0-.5-.3-1.4-1-.6-.5-.7-1-.5-1.4z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Runner</p>
        </article>
        <article>
            <figure class="flex items-center justify-center w-20 h-20 p-3 mx-auto rounded-full bg-indigo-400 text-indigo-900">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" class="h-16 fill-current">
                    <path d="M14.4 26.3a1.33 1.33 0 0 0 1.34-1.3v-1.71H21V25a1.33 1.33 0 1 0 2.67 0v-3a1.33 1.33 0 0 0-1.33-1.33h-8A1.33 1.33 0 0 0 13.07 22v3a1.33 1.33 0 0 0 1.33 1.3z"></path>
                    <path d="M87.92 26.71H66.06l-3.7-8.59a6.44 6.44 0 0 0-5.92-3.9H43.51a6.44 6.44 0 0 0-5.92 3.9l-3.7 8.59H12.08A7.09 7.09 0 0 0 5 33.79V78.7a7.09 7.09 0 0 0 7.08 7.08h75.84A7.09 7.09 0 0 0 95 78.7V33.79a7.09 7.09 0 0 0-7.08-7.08zM40 19.18a3.78 3.78 0 0 1 3.47-2.28h12.97a3.78 3.78 0 0 1 3.47 2.28l3.25 7.53H36.79zm47.92 63.93H12.08a4.42 4.42 0 0 1-4.41-4.41V42.92h14.69a1.33 1.33 0 1 0 0-2.67H7.67v-6.46a4.42 4.42 0 0 1 4.41-4.41h75.84a4.42 4.42 0 0 1 4.41 4.41v6.46h-14.9a1.33 1.33 0 1 0 0 2.67h14.9V78.7a4.42 4.42 0 0 1-4.41 4.41z"></path>
                    <path d="M50 31a25.23 25.23 0 1 0 25.23 25.24A25.26 25.26 0 0 0 50 31zm0 47.43a22.2 22.2 0 1 1 22.2-22.2A22.22 22.22 0 0 1 50 78.44z"></path>
                    <path d="M50 36.33a19.91 19.91 0 1 0 19.91 19.91A19.94 19.94 0 0 0 50 36.33zm0 36.79a16.88 16.88 0 1 1 16.88-16.88A16.9 16.9 0 0 1 50 73.12zm5.21-52.65H44.79a1.33 1.33 0 0 0 0 2.67h10.42a1.33 1.33 0 0 0 0-2.67z"></path>
                </svg>
            </figure>
            <p class="mt-2 text-center">Photographer</p>
        </article>
    </section>

    <section class="px-4 mx-auto max-w-3xl">

        <header class="my-6 text-white text-2xl">
            <p>I've been working in the web industry for the past {{date('Y') - 2001}} years.</p>
            <p class="text-purple-100">Having experienced many different technologies; I've settled to focus on
                these four terrific tools for building modern web applications:</p>
        </header>

        <article id="technologies" class="flex flex-wrap justify-around sm:justify-between">
            <article class="text-center p-4 h-48 flex flex-col justify-around items-center">
                <figure class="flex items-center h-full">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 206 105" class="h-16">
                        <g fill="none" fill-rule="evenodd">
                            <g id="php" fill-rule="nonzero">
                                <ellipse id="Oval" cx="103" cy="52.17" fill="#8892BF" rx="103" ry="52.17"></ellipse>
                                <path id="Shape" fill="#1B2935"
                                      d="M58.45 36.51c4.69 0 7.79.84 9.38 2.6 1.59 1.76 1.93 4.69 1.09 8.87-.84 4.36-2.51 7.54-4.94 9.38-2.43 1.84-6.12 2.77-11.06 2.77h-7.45l4.6-23.62h8.38zM28.47 84.07h12.31l2.93-14.98h10.55c4.7 0 8.46-.5 11.48-1.51a22 22 0 0 0 8.2-4.94 25.13 25.13 0 0 0 5.03-6.28 24.43 24.43 0 0 0 2.68-7.54c1.34-6.7.33-11.9-2.93-15.66-3.27-3.77-8.46-5.61-15.58-5.61h-23.7L28.47 84.07zm63.06-71.5h12.22l-2.93 14.98h10.89c6.87 0 11.56 1.17 14.15 3.6 2.6 2.43 3.35 6.28 2.35 11.64l-5.11 26.3h-12.4l4.86-25.04c.59-2.85.34-4.78-.59-5.78-.92-1-3.01-1.6-6.1-1.6h-9.8L92.77 69.1H80.56l10.97-56.53zM154.5 36.5c4.69 0 7.79.84 9.38 2.6 1.59 1.76 1.92 4.69 1.09 8.87-.84 4.36-2.51 7.54-4.94 9.38-2.43 1.84-6.12 2.77-11.06 2.77h-7.45l4.6-23.62h8.38zm-29.98 47.56h12.31l2.93-14.98h10.55c4.7 0 8.46-.5 11.48-1.51a22 22 0 0 0 8.2-4.94 25.13 25.13 0 0 0 5.03-6.28 24.43 24.43 0 0 0 2.68-7.54c1.34-6.7.33-11.9-2.93-15.66-3.27-3.77-8.46-5.61-15.58-5.61h-23.7l-10.97 56.52z"></path>
                            </g>
                        </g>
                    </svg>
                </figure>
                <span class="px-3 py-1 bg-purple-600 text-purple-100 rounded-full">PHP</span>
            </article>

            <article class="text-center p-4 h-48 flex flex-col justify-around items-center">
                <figure class="flex items-center h-full">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 52" class="h-16">
                        <path d="M49.626 11.564a.809.809 0 0 1 .028.209v10.972a.8.8 0 0 1-.402.694l-9.209 5.302V39.25c0 .286-.152.55-.4.694L20.42 51.01c-.044.025-.092.041-.14.058-.018.006-.035.017-.054.022a.805.805 0 0 1-.41 0c-.022-.006-.042-.018-.063-.026-.044-.016-.09-.03-.132-.054L.402 39.944A.801.801 0 0 1 0 39.25V6.334c0-.072.01-.142.028-.21.006-.023.02-.044.028-.067.015-.042.029-.085.051-.124.015-.026.037-.047.055-.071.023-.032.044-.065.071-.093.023-.023.053-.04.079-.06.029-.024.055-.05.088-.069h.001l9.61-5.533a.802.802 0 0 1 .8 0l9.61 5.533h.002c.032.02.059.045.088.068.026.02.055.038.078.06.028.029.048.062.072.094.017.024.04.045.054.071.023.04.036.082.052.124.008.023.022.044.028.068a.809.809 0 0 1 .028.209v20.559l8.008-4.611v-10.51c0-.07.01-.141.028-.208.007-.024.02-.045.028-.068.016-.042.03-.085.052-.124.015-.026.037-.047.054-.071.024-.032.044-.065.072-.093.023-.023.052-.04.078-.06.03-.024.056-.05.088-.069h.001l9.611-5.533a.801.801 0 0 1 .8 0l9.61 5.533c.034.02.06.045.09.068.025.02.054.038.077.06.028.029.048.062.072.094.018.024.04.045.054.071.023.039.036.082.052.124.009.023.022.044.028.068zm-1.574 10.718v-9.124l-3.363 1.936-4.646 2.675v9.124l8.01-4.611zm-9.61 16.505v-9.13l-4.57 2.61-13.05 7.448v9.216l17.62-10.144zM1.602 7.719v31.068L19.22 48.93v-9.214l-9.204-5.209-.003-.002-.004-.002c-.031-.018-.057-.044-.086-.066-.025-.02-.054-.036-.076-.058l-.002-.003c-.026-.025-.044-.056-.066-.084-.02-.027-.044-.05-.06-.078l-.001-.003c-.018-.03-.029-.066-.042-.1-.013-.03-.03-.058-.038-.09v-.001c-.01-.038-.012-.078-.016-.117-.004-.03-.012-.06-.012-.09v-.002-21.481L4.965 9.654 1.602 7.72zm8.81-5.994L2.405 6.334l8.005 4.609 8.006-4.61-8.006-4.608zm4.164 28.764l4.645-2.674V7.719l-3.363 1.936-4.646 2.675v20.096l3.364-1.937zM39.243 7.164l-8.006 4.609 8.006 4.609 8.005-4.61-8.005-4.608zm-.801 10.605l-4.646-2.675-3.363-1.936v9.124l4.645 2.674 3.364 1.937v-9.124zM20.02 38.33l11.743-6.704 5.87-3.35-8-4.606-9.211 5.303-8.395 4.833 7.993 4.524z"
                              fill="#FF2D20" fill-rule="evenodd"/>
                    </svg>
                </figure>
                <span class="px-3 py-1 bg-purple-600 text-purple-100 rounded-full">Laravel</span>
            </article>

            <article class="text-center p-4 h-48 flex flex-col justify-around items-center">
                <figure class="flex items-center h-full">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256" class="h-16">
                        <path d="M0 0h256v256H0V0z" fill="#F7DF1E"></path>
                        <path d="M67.312 213.932l19.59-11.856c3.78 6.701 7.218 12.371 15.465 12.371 7.905 0 12.89-3.092 12.89-15.12v-81.798h24.057v82.138c0 24.917-14.606 36.259-35.916 36.259-19.245 0-30.416-9.967-36.087-21.996M152.381 211.354l19.588-11.341c5.157 8.421 11.859 14.607 23.715 14.607 9.969 0 16.325-4.984 16.325-11.858 0-8.248-6.53-11.17-17.528-15.98l-6.013-2.58c-17.357-7.387-28.87-16.667-28.87-36.257 0-18.044 13.747-31.792 35.228-31.792 15.294 0 26.292 5.328 34.196 19.247L210.29 147.43c-4.125-7.389-8.591-10.31-15.465-10.31-7.046 0-11.514 4.468-11.514 10.31 0 7.217 4.468 10.14 14.778 14.608l6.014 2.577c20.45 8.765 31.963 17.7 31.963 37.804 0 21.654-17.012 33.51-39.867 33.51-22.339 0-36.774-10.654-43.819-24.574"></path>
                    </svg>
                </figure>
                <span class="px-3 py-1 bg-purple-600 text-purple-100 rounded-full">Javascript</span>
            </article>

            <article class="text-center p-4 h-48 flex flex-col justify-around items-center">
                <figure class="flex items-center h-full">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 256" class="h-16">
                        <path d="M204.8 0H256L128 220.8 0 0h97.92L128 51.2 157.44 0h47.36z" fill="#41B883"></path>
                        <path d="M0 0l128 220.8L256 0h-51.2L128 132.48 50.56 0H0z" fill="#41B883"></path>
                        <path d="M50.56 0L128 133.12 204.8 0h-47.36L128 51.2 97.92 0H50.56z" fill="#35495E"></path>
                    </svg>
                </figure>
                <span class="px-3 py-1 bg-purple-600 text-purple-100 rounded-full">Vue.js</span>
            </article>
        </article>
    </section>

    <section class="sm:flex items-center max-w-3xl mx-3 my-6 mt-6 lg:-ml-32 text-2xl text-yellow-500">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 178 178"
             class="w-64 mx-auto sm:mr-4 flex-shrink-0 text-indigo-600 stroke-current" style="transform: rotate(5deg)">
            <defs>
                <style>.cls-1 {
                        fill: none;
                        stroke-linecap: round;
                        stroke-linejoin: round;
                    }</style>
            </defs>
            <path class="cls-1"
                  d="M174,26,155.2,46.93a4.48,4.48,0,0,1-3.33,1.48H26.13a4.49,4.49,0,0,1-3.33-1.48L4,26Z"></path>
            <path class="cls-1"
                  d="M111.37,48.41V59.59a2.24,2.24,0,0,1-2.24,2.24H68.87a2.24,2.24,0,0,1-2.24-2.24V48.41Z"></path>
            <path class="cls-1"
                  d="M174,23.8V26H4V23.8a2.23,2.23,0,0,1,2.24-2.23H171.76A2.23,2.23,0,0,1,174,23.8Z"></path>
            <path class="cls-1" d="M162.4,78.44a2.24,2.24,0,0,0,1.48-4.22l-27.42-9.68-5,.62,3.47,3.6Z"></path>
            <circle class="cls-1" cx="29.72" cy="93.14" r="11.18"></circle>
            <path class="cls-1"
                  d="M115.84,106.57H62.16a4.49,4.49,0,0,1-4.48-4.48V82a2.24,2.24,0,0,1,2.24-2.24h58.16A2.24,2.24,0,0,1,120.32,82v20.13A4.49,4.49,0,0,1,115.84,106.57Z"></path>
            <line class="cls-1" x1="64.4" y1="99.85" x2="68.87" y2="99.85"></line>
            <line class="cls-1" x1="64.4" y1="93.14" x2="68.87" y2="93.14"></line>
            <line class="cls-1" x1="64.4" y1="86.43" x2="68.87" y2="86.43"></line>
            <line class="cls-1" x1="73.34" y1="93.14" x2="77.82" y2="93.14"></line>
            <line class="cls-1" x1="73.34" y1="86.43" x2="77.82" y2="86.43"></line>
            <line class="cls-1" x1="82.29" y1="93.14" x2="86.76" y2="93.14"></line>
            <line class="cls-1" x1="82.29" y1="86.43" x2="86.76" y2="86.43"></line>
            <line class="cls-1" x1="91.24" y1="93.14" x2="95.71" y2="93.14"></line>
            <line class="cls-1" x1="91.24" y1="86.43" x2="95.71" y2="86.43"></line>
            <line class="cls-1" x1="100.18" y1="93.14" x2="104.66" y2="93.14"></line>
            <line class="cls-1" x1="100.18" y1="86.43" x2="104.66" y2="86.43"></line>
            <line class="cls-1" x1="109.13" y1="99.85" x2="113.61" y2="99.85"></line>
            <line class="cls-1" x1="109.13" y1="93.14" x2="113.61" y2="93.14"></line>
            <line class="cls-1" x1="109.13" y1="86.43" x2="113.61" y2="86.43"></line>
            <line class="cls-1" x1="73.34" y1="99.85" x2="104.66" y2="99.85"></line>
            <line class="cls-1" x1="145.98" y1="95.57" x2="144.51" y2="99.8"></line>
            <path class="cls-1"
                  d="M135.29,102.52a7.83,7.83,0,1,0,14.8,5.12l2.19-6.34a7.83,7.83,0,0,0-14.8-5.12Z"></path>
            <line class="cls-1" x1="82.04" y1="120.19" x2="86.4" y2="121.24"></line>
            <path class="cls-1"
                  d="M74.76,150.65,55.18,146a2.23,2.23,0,0,1-1.65-2.7l6.24-26.1a2.24,2.24,0,0,1,2.69-1.66L82,120.19Z"></path>
            <path class="cls-1"
                  d="M107.63,128.61l-2.58,10.79-3-4.83-3.8,2.36h0l5.33,8.6-2.19,9.19a2.23,2.23,0,0,1-2.7,1.65l-19.58-4.68,1-4.35,1-4.36,1-4.34,1-4.35,1-4.36,1-4.35,1-4.34L106,125.92A2.23,2.23,0,0,1,107.63,128.61Z"></path>
            <line class="cls-1" x1="78.83" y1="124.03" x2="87.53" y2="126.11"></line>
            <line class="cls-1" x1="77.79" y1="128.38" x2="86.49" y2="130.46"></line>
            <line class="cls-1" x1="76.75" y1="132.73" x2="85.45" y2="134.81"></line>
            <line class="cls-1" x1="75.71" y1="137.08" x2="84.41" y2="139.16"></line>
            <line class="cls-1" x1="74.67" y1="141.43" x2="83.37" y2="143.51"></line>
            <line class="cls-1" x1="73.63" y1="145.78" x2="82.33" y2="147.86"></line>
            <line class="cls-1" x1="74.76" y1="150.65" x2="79.11" y2="151.69"></line>
            <polygon class="cls-1"
                     points="98.26 136.93 98.26 136.91 96.62 130.05 102.05 134.56 102.06 134.57 98.26 136.93"></polygon>
            <path class="cls-1"
                  d="M112,154.77h0a2.24,2.24,0,0,1-3.08-.73l-1.18-1.9,3.8-2.35,1.18,1.9A2.24,2.24,0,0,1,112,154.77Z"></path>
            <polygon class="cls-1"
                     points="111.47 149.77 107.67 152.12 103.58 145.53 98.25 136.93 98.26 136.93 102.06 134.57 105.05 139.4 111.47 149.77"></polygon>
            <polygon class="cls-1"
                     points="102.06 134.57 98.26 136.93 98.26 136.91 102.05 134.56 102.06 134.57"></polygon>
        </svg>
        <p class="after:border-yellow-left">I love to partner with those that have
            <mark class="marker">bloomin' brilliant</mark>
            ideas that need a little expertise and experience to
            guide them into existence
        </p>
    </section>

    <section class="max-w-2xl mx-3 my-6 lg:-mr-32 sm:flex items-center text-2xl text-red-500 text-right">
        <p class="after:border-red-right">If you need that
            <mark class="marker">enthusiast on-demand</mark>
            hand with
            your Laravel or Vue projects perhaps we'd be a good match?
        </p>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 112"
             class="w-48 mx-auto sm:ml-6 flex-shrink-0 text-indigo-600 stroke-current">
            <defs>
                <style>.cls-1 {
                        fill: none;
                        stroke-linecap: round;
                        stroke-linejoin: round;
                    }</style>
            </defs>

            <path class="cls-1" d="M47.88,45.93v2.3H36V43.07a2,2,0,0,1,2-2h3.39"></path>
            <path class="cls-1" d="M19.7,41.08H11.76a2,2,0,0,0-2,2v5.16H21.68V43.06A2,2,0,0,0,19.7,41.08Z"></path>
            <path class="cls-1" d="M7,67.28a2,2,0,0,0-2,2V84.35a2,2,0,0,0,2,2H24.53"></path>
            <line class="cls-1" x1="52.64" y1="53.98" x2="52.64" y2="49.46"></line>
            <path class="cls-1" d="M7,67.28a2,2,0,0,1-2-2V50.21a2,2,0,0,1,2-2H51"></path>
            <path class="cls-1"
                  d="M58.75,54l-6.11-4.52L34.4,35.94A2,2,0,0,1,34,33.16L43,21a2,2,0,0,1,2.78-.41l35.1,26a2,2,0,0,1,.41,2.78L77.85,54"></path>
            <path class="cls-1"
                  d="M60.2,22.45l-6.38-4.73a2,2,0,0,0-2.78.41L48,22.28l9.57,7.09,3.07-4.15A2,2,0,0,0,60.2,22.45Z"></path>
            <path class="cls-1"
                  d="M74.87,33.32l6.38,4.73a2,2,0,0,1,.41,2.78L78.59,45,69,37.88l3.07-4.14A2,2,0,0,1,74.87,33.32Z"></path>
            <path class="cls-1"
                  d="M93,94.67H31.48a2,2,0,0,1-2-2V60.93a2,2,0,0,1,2-2H93a2,2,0,0,1,2,2V92.68A2,2,0,0,1,93,94.67Z"></path>
            <circle class="cls-1" cx="54.8" cy="69.36" r="4.47"></circle>
            <circle class="cls-1" cx="39.92" cy="69.36" r="4.47"></circle>
            <circle class="cls-1" cx="39.92" cy="84.25" r="4.47"></circle>
            <circle class="cls-1" cx="69.69" cy="69.36" r="4.47"></circle>
            <circle class="cls-1" cx="84.58" cy="69.36" r="4.47"></circle>
            <circle class="cls-1" cx="54.8" cy="84.25" r="4.47"></circle>
            <circle class="cls-1" cx="69.69" cy="84.25" r="4.47"></circle>
            <circle class="cls-1" cx="84.58" cy="84.25" r="4.47"></circle>
            <line class="cls-1" x1="24.53" y1="67.28" x2="6.98" y2="67.28"></line>
        </svg>
    </section>

    <section class="mt-8 p-4 rounded text-white text-center">
        <p class="text-2xl sm:text-3xl">I'd really like to know what kind of project or problem you're dealing&nbsp;with</p>
        <mark class="marker text-3xl">Let's figure out how we can work together</mark>

        <section class="flex flex-wrap lg:flex-no-wrap justify-center sm:justify-around m-6 twist text-left leading-tight">
            <a href="https://t.me/richstandbrook" class="flex items-center pr-6 bg-purple-600 rounded-full text-xl">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" class="w-16 mr-2">
                    <path fill="#29b6f6" d="M24 4A20 20 0 1 0 24 44A20 20 0 1 0 24 4Z"></path>
                    <path fill="#fff"
                          d="M33.95,15l-3.746,19.126c0,0-0.161,0.874-1.245,0.874c-0.576,0-0.873-0.274-0.873-0.274l-8.114-6.733 l-3.97-2.001l-5.095-1.355c0,0-0.907-0.262-0.907-1.012c0-0.625,0.933-0.923,0.933-0.923l21.316-8.468 c-0.001-0.001,0.651-0.235,1.126-0.234C33.667,14,34,14.125,34,14.5C34,14.75,33.95,15,33.95,15z"></path>
                    <path fill="#b0bec5"
                          d="M23,30.505l-3.426,3.374c0,0-0.149,0.115-0.348,0.12c-0.069,0.002-0.143-0.009-0.219-0.043 l0.964-5.965L23,30.505z"></path>
                    <path fill="#cfd8dc"
                          d="M29.897,18.196c-0.169-0.22-0.481-0.26-0.701-0.093L16,26c0,0,2.106,5.892,2.427,6.912 c0.322,1.021,0.58,1.045,0.58,1.045l0.964-5.965l9.832-9.096C30.023,18.729,30.064,18.416,29.897,18.196z"></path>
                </svg>
                <p>
                    Let's chat on Telegram<br>
                    t.me/richstandbrook
                </p>
            </a>
            <a href="https://t.me/richstandbrook"
               class="flex items-center pl-2 pr-6 bg-purple-600 rounded-full text-xl">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" class="w-16">
                    <path fill="#03A9F4"
                          d="M42,12.429c-1.323,0.586-2.746,0.977-4.247,1.162c1.526-0.906,2.7-2.351,3.251-4.058c-1.428,0.837-3.01,1.452-4.693,1.776C34.967,9.884,33.05,9,30.926,9c-4.08,0-7.387,3.278-7.387,7.32c0,0.572,0.067,1.129,0.193,1.67c-6.138-0.308-11.582-3.226-15.224-7.654c-0.64,1.082-1,2.349-1,3.686c0,2.541,1.301,4.778,3.285,6.096c-1.211-0.037-2.351-0.374-3.349-0.914c0,0.022,0,0.055,0,0.086c0,3.551,2.547,6.508,5.923,7.181c-0.617,0.169-1.269,0.263-1.941,0.263c-0.477,0-0.942-0.054-1.392-0.135c0.94,2.902,3.667,5.023,6.898,5.086c-2.528,1.96-5.712,3.134-9.174,3.134c-0.598,0-1.183-0.034-1.761-0.104C9.268,36.786,13.152,38,17.321,38c13.585,0,21.017-11.156,21.017-20.834c0-0.317-0.01-0.633-0.025-0.945C39.763,15.197,41.013,13.905,42,12.429"></path>
                </svg>
                <p>
                    Tweet me<br>
                    @richstandbrook
                </p>
            </a>
        </section>
    </section>
@endsection

@push('scripts')
    <script>
        /*
        https://codepen.io/GreenSock/pen/gyWrPO
        pass in an object with any of the following optional properties (just like the stagger special object):
        {
          amount: amount (in seconds) that should be distributed
          from: "center" | "end" | "edges" | start" | index value (integer)
          ease: any ease, like "power1"
          axis: "x" | "y" (or omit, and it'll be based on both the x and y positions)
        }
        */
        function distributeByPosition(vars) {
            var ease = vars.ease,
                from = vars.from || 0,
                base = vars.base || 0,
                axis = vars.axis,
                ratio = {center: 0.5, end: 1, edges: 0.5}[from] || 0,
                distances;
            return function (i, target, a) {
                var l = a.length,
                    originX, originY, x, y, d, j, minX, maxX, minY, maxY, positions;
                if (!distances) {
                    distances = [];
                    minX = minY = Infinity;
                    maxX = maxY = -minX;
                    positions = [];
                    for (j = 0; j < l; j++) {
                        d = a[j].getBoundingClientRect();
                        x = (d.left + d.right) / 2; //based on the center of each element
                        y = (d.top + d.bottom) / 2;
                        if (x < minX) {
                            minX = x;
                        }
                        if (x > maxX) {
                            maxX = x;
                        }
                        if (y < minY) {
                            minY = y;
                        }
                        if (y > maxY) {
                            maxY = y;
                        }
                        positions[j] = {x: x, y: y};
                    }
                    originX = isNaN(from) ? minX + (maxX - minX) * ratio : positions[from].x || 0;
                    originY = isNaN(from) ? minY + (maxY - minY) * ratio : positions[from].y || 0;
                    maxX = 0;
                    minX = Infinity;
                    for (j = 0; j < l; j++) {
                        x = positions[j].x - originX;
                        y = originY - positions[j].y;
                        distances[j] = d = !axis ? Math.sqrt(x * x + y * y) : Math.abs((axis === "y") ? y : x);
                        if (d > maxX) {
                            maxX = d;
                        }
                        if (d < minX) {
                            minX = d;
                        }
                    }
                    distances.max = maxX - minX;
                    distances.min = minX;
                    distances.v = l = (vars.amount || (vars.each * l) || 0) * (from === "edges" ? -1 : 1);
                    distances.b = (l < 0) ? base - l : base;
                }
                l = (distances[i] - distances.min) / distances.max;
                return distances.b + (ease ? ease.getRatio(l) : l) * distances.v;
            };
        }

        document.getElementById('attributes').classList.remove('hidden');

        gsap.from("#attributes article", {
            // rotation: 180,
            scale: 0,
            y: 200,
            opacity: 0,
            ease: "elastic.out(1.5, 0.5)",
            duration: .8,
            delay: .3,
            stagger: distributeByPosition({
                from: "center",
                amount: .3
            })
        });

        gsap.from("#technologies article", {
            // rotation: 180,
            scale: 0,
            opacity: 0,
            ease: "elastic.out(1.5, 0.5)",
            duration: 1,
            delay: .4,
            stagger: distributeByPosition({
                from: "center",
                amount: .3
            })
        });
    </script>
@endpush

