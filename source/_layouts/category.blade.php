@extends('_layouts.master')

@push('meta')
    <meta property="og:title" content="{{ $page->title }}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="{{ $page->description }}" />
@endpush

@section('body')
    <header class="max-w-5xl mx-auto mt-24 text-white">
        <h1>{{ $page->title }}</h1>
    </header>

    <section class="text-2xl max-w-5xl mx-auto mb-12 text-purple-100">
        @yield('content')
    </section>

    <section class="flex flex-wrap max-w-5xl mx-auto">
        @foreach ($page->posts($posts) as $post)
            <article class="w-1/2">
                @include('_components.post-preview-inline')
            </article>
        @endforeach
    </section>

    @include('_components.newsletter-signup')
@stop
