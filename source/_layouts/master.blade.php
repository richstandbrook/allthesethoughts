<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="{{ $page->meta_description ?? $page->siteDescription }}">

    <meta property="og:title" content="{{ $page->title ?  $page->title . ' | ' : '' }}{{ $page->siteName }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="{{ $page->getUrl() }}"/>
    <meta property="og:description" content="{{ $page->siteDescription }}"/>

    <title>{{ $page->siteName }}{{ $page->title ? ' | ' . $page->title : '' }}</title>

    <link rel="home" href="{{ $page->baseUrl }}">
    <link rel="icon" href="/favicon.ico">
    <link href="/blog/feed.atom" type="application/atom+xml" rel="alternate" title="{{ $page->siteName }} Atom Feed">

    @stack('meta')

    <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
</head>

<body class="flex flex-col justify-between min-h-screen font-sans bg-purple-400">
<header id="header"
        role="banner"
        class="fixed z-30 top-0 flex items-center h-24 w-full py-4 bg-gradient-purple bg-no-repeat transition"
        :class="{'bg-offtop': scrollPosition < 80}"
>
    <div class="container flex items-center max-w-8xl mx-auto px-4 lg:px-8">
        <div class="flex items-center">
            <a href="/" title="{{ $page->siteName }} home" class="inline-flex items-center">
                <h1 class="text-lg md:text-2xl text-white font-extrabold">{{ $page->siteName }}</h1>
            </a>
        </div>

        <div id="vue-search" class="flex flex-1 justify-end items-center">
            <search></search>

            @include('_nav.menu')

            @include('_nav.menu-toggle')
        </div>
    </div>
</header>

@include('_nav.menu-responsive')

<main role="main" class="flex flex-col items-center justify-center flex-auto w-full">
    @yield('body')
</main>

<footer class="relative overflow-hidden text-center text-sm mt-12 py-8 text-purple-200" role="contentinfo">
    <div class="relative z-10">
        <p class="text-lg my-4 text-purple-100">
            Carry on the conversation
            <a href="https://twitter.com/richstandbrook" title="Talk on Twitter" class="text-white">@richstandbrook</a>
        </p>
        <p>
            &copy; Rich Standbrook {{ date('Y') }}.<br class="sm:hidden">
            Built with <a href="http://jigsaw.tighten.co" title="Jigsaw by Tighten"  class="text-purple-100">Jigsaw</a>
            and <a href="https://tailwindcss.com" title="Tailwind CSS, a utility-first CSS framework"  class="text-purple-100">Tailwind CSS</a>.
        </p>
    </div>
    @include('_components.long-line', ['class' => 'text-blue-500 w-20 absolute left-0 bottom-0 -ml-4 -mb-2'])
    @include('_components.line', ['class' => 'text-red-500 w-16 absolute left-0 bottom-0 ml-5 -mb-8'])
    @include('_components.joined-lines', ['class' => 'text-purple-500 w-40 absolute top-0 right-0 -mr-10'])
</footer>

<script src="{{ mix('js/main.js', 'assets/build') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/gsap@3.0.2/dist/gsap.min.js"></script>

@stack('scripts')
@if ($page->production)
<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setDomains", ["*.allthesethoughts.uk","*.allthesethoughts.com","*.ihaveallthesethoughtsandimprettysuretheyallcontradicteachother.com"]]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="https://analytics.milk.media/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '1']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="https://analytics.milk.media/matomo.php?idsite=1&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->
@endif
</body>
</html>
