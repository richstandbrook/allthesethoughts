<div class="flex flex-col m-4">
    <p class="text-purple-200 font-medium my-2">
        {{ $post->getDate()->format('F j, Y') }}
    </p>

    <h2 class="text-2xl leading-none">
        <a href="{{ $post->getUrl() }}"
           title="Read more - {{ $post->title }}"
           class="text-orange-500 hover:text-orange-600 font-extrabold"
        >{{ $post->title }}</a>
    </h2>

    <p class="mb-4 text-purple-100">
        {!! $post->getExcerpt(200) !!}
        <a href="{{ $post->getUrl() }}"
           title="Read more - {{ $post->title }}"
           class="text-white font-semibold tracking-wide mb-2"
        >Keep reading</a>
    </p>
</div>
