<form action="https://allthesethoughts.us4.list-manage.com/subscribe/post?u=a8c3689905ba873012f3e76cd&amp;id=1241c95f9a" method="post" class="flex flex-wrap justify-center text-center">
    <h2 class="w-full text-2xl text-purple-100 mb-2">Some thoughts by email?</h2>
    <div class="flex">
        <label for="email" class="hidden">Email Address</label>
        <input type="email" name="EMAIL" class="sm:w-64 bg-white p-4 rounded-l" id="email" placeholder="Email address">
        <button type="submit" class="text-red-900 bg-red-500 font-bold py-4 px-6 rounded-r">Subscribe</button>
    </div>
</form>
