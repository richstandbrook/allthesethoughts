<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 35"
     class="fill-current {{$class ?? null}}"
     data-rellax-speed="{{$rellaxSpeed ?? null}}"
     data-rellax-percentage="{{$rellaxPercentage ?? null}}">
    <path d="M29.324 10.303c-.315.815-.669 2.527-1.557 3.892a580.832 580.832 0 0 1-11.024 16.311c-2.376 3.395-6.017 4.648-9.794 3.66C3.19 33.181.598 30.241.07 26.238c-.291-2.2.348-4.253 1.548-6.058C5.204 14.791 8.805 9.408 12.523 4.11 15.08.465 19.09-.806 23.05.5c3.764 1.238 6.357 4.934 6.274 9.804"/>
</svg>